
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Puff Gabriel
 */
public class SQLExecutor extends JFrame implements ActionListener {

    private JPanel mainPanel;
    private JTextField tf_sql;
    private JTextArea ta_result;
    private JButton bt_execute;
    private JButton bt_settings;
    private JLabel lb_sql;
    private JLabel lb_result;
    private JLabel lb_title;
    private JTable table;
    private DefaultTableModel model;

    public SQLExecutor() {
        initialize();
    }

    private void initialize() {
        this.setSize(new Dimension(600, 600));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        mainPanel = new JPanel();
        this.setContentPane(mainPanel);
        mainPanel.setLayout(null);

        lb_title = new JLabel();
        lb_title.setText("SQL-Executor");
        lb_title.setFont(new Font("Serif", Font.PLAIN, 50));
        lb_title.setBounds(150, 15, 400, 50);
        mainPanel.add(lb_title);

        lb_sql = new JLabel();
        lb_sql.setText("SQL:");
        lb_sql.setBounds(50, 80, 60, 40);
        mainPanel.add(lb_sql);

        tf_sql = new JTextField();
        tf_sql.setBounds(100, 80, 350, 40);
        mainPanel.add(tf_sql);

        bt_execute = new JButton("Execute");
        bt_execute.setBounds(450, 80, 100, 38);
        bt_execute.setBackground(Color.WHITE);
        bt_execute.setForeground(Color.BLACK);
        Border line = new LineBorder(Color.BLACK);
        Border margin = new EmptyBorder(5, 15, 5, 15);
        Border compound = new CompoundBorder(line, margin);
        bt_execute.setBorder(compound);
        bt_execute.addActionListener(this);
        mainPanel.add(bt_execute);

        bt_settings = new JButton("Settings");
        bt_settings.setBounds(470, 25, 80, 38);
        bt_settings.setBackground(Color.WHITE);
        bt_settings.setForeground(Color.BLACK);
        line = new LineBorder(Color.BLACK);
        margin = new EmptyBorder(5, 15, 5, 15);
        compound = new CompoundBorder(line, margin);
        bt_settings.setBorder(compound);
        bt_settings.addActionListener(this);
        mainPanel.add(bt_settings);

        table = new JTable();
        model = new DefaultTableModel();
        table.setModel(model);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.setFillsViewportHeight(true);
        JScrollPane pane = new JScrollPane(table);
        pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        pane.setBounds(15, 150, 550, 400);
        mainPanel.add(pane);

    }

    public static void main(String[] args) {
        SQLExecutor f = new SQLExecutor();
        f.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource().equals(bt_execute)) {

            model = new DefaultTableModel();
            table.setModel(model);
            Database db = new Database();
            Connection conn = db.getConnection();
            try {
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(tf_sql.getText());
                int colNr = rs.getMetaData().getColumnCount();
                String[] colNames = new String[colNr];
                for (int i = 1; i <= colNr; i++) {
                    colNames[i - 1] = rs.getMetaData().getColumnLabel(i);
                }
                model.setColumnIdentifiers(colNames);
                while (rs.next()) {
                    Object[] rowData = new Object[colNr];
                    for (int i = 1; i <= colNr; i++) {
                        rowData[i - 1] = rs.getObject(i);
                    }
                    model.addRow(rowData);
                }
                rs.close();
                st.close();
                conn.close();

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Fehler:\nBitte überprüfen Sie die SQL-Syntax!");
                ex.printStackTrace();
            }

        } else if (e.getSource().equals(bt_settings)) {
            Settings s = new Settings();
            s.setVisible(true);
        }

    }

}
