/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliothek;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Puff Gabriel
 */
public class Startpage extends JFrame implements ActionListener {

    private JPanel mainPanel;

    private JLabel mainlbl;
    private JButton btn_buch;
    private JButton btn_schueler;
    private JButton btn_ausleih;
    private JButton btn_rückgabe;
    private JButton btn_suchen;


    public Startpage() {
        initialize();
    }

    private void initialize() {
        this.setSize(new Dimension(800, 600));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        mainPanel = new JPanel();
        this.setContentPane(mainPanel);
        mainPanel.setLayout(null);

        mainlbl = new JLabel();
        mainlbl.setText("Gabriels Bibliothek");
        mainlbl.setFont(new Font("Arial", Font.PLAIN, 50));
        mainlbl.setBounds(175, 50, 500, 50);
        mainPanel.add(mainlbl);

        btn_buch = new JButton();
        btn_buch.setBounds(50, 350, 100, 50);
        btn_buch.setText("Bücher");
        btn_buch.addActionListener(this);
        mainPanel.add(btn_buch);

        btn_schueler = new JButton();
        btn_schueler.setBounds(200, 350, 100, 50);
        btn_schueler.setText("Schüler");
        btn_schueler.addActionListener(this);
        mainPanel.add(btn_schueler);

        btn_ausleih = new JButton();
        btn_ausleih.setBounds(350, 350, 100, 50);
        btn_ausleih.setText("Ausleih");
        btn_ausleih.addActionListener(this);
        mainPanel.add(btn_ausleih);

        btn_rückgabe = new JButton();
        btn_rückgabe.setBounds(500, 350, 100, 50);
        btn_rückgabe.setText("Rückgabe");
        btn_rückgabe.addActionListener(this);
        mainPanel.add(btn_rückgabe);

        btn_suchen = new JButton();
        btn_suchen.setBounds(650, 350, 100, 50);
        btn_suchen.setText("Suchen");
        btn_suchen.addActionListener(this);
        mainPanel.add(btn_suchen);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
//        DBconn db = new DBconn();
//        Connection conn = db.getConnetion();
        
        if (e.getSource().equals(btn_buch)) {
            Buch_menu BM = new Buch_menu();
            BM.setVisible(true);
        } else if (e.getSource().equals(btn_schueler)) {
            Schueler_menu SM = new Schueler_menu();
            SM.setVisible(true);
        } else if (e.getSource().equals(btn_ausleih)) {
            Ausleih_menu AM = new Ausleih_menu();
            AM.setVisible(true);
        } else if (e.getSource().equals(btn_rückgabe)) {
            Rueckgabe_menu RM = new Rueckgabe_menu();
            RM.setVisible(true);
        } else if (e.getSource().equals(btn_suchen)) {
            Search_menu SRM = new Search_menu();
            SRM.setVisible(true);
        }
//        
//        try {
//            conn.close();
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//        }
    }
    
    
}
