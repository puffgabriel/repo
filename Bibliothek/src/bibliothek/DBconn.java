/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliothek;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Puff Gabriel
 */
public class DBconn {

    private Connection CONNECTION = null;
    private boolean connected = false;

    
    public boolean isConnected(){
        return this.connected;
    }
    
    public boolean connect() {
        try {
            Class.forName("org.postgresql.Driver");//"org.postgresql.Driver"
            if (CONNECTION == null) {
                CONNECTION = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/Bibliothek", "postgres", "root");
            }
            connected = true;
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return false;
    }

    public Connection getConnetion() {
        return this.CONNECTION;
    }

    public int executeUpdate(String sql) {

        try {
            Statement statement = CONNECTION.createStatement();
            return statement.executeUpdate(sql);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return -1;
    }

    public String printResultSet(ResultSet answer) {

        String ausgabe = "";

        try {
            for (int i = 1; i <= answer.getMetaData().getColumnCount(); i++) {
                ausgabe += (answer.getMetaData().getColumnName(i) + "\t|");
            }
            ausgabe += ("\n--------------------------\n");

            while (answer.next()) {

                for (int i = 1; i <= answer.getMetaData().getColumnCount(); i++) {
                    ausgabe += (answer.getString(i) + "\t|");
                }
                ausgabe += "\n";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        System.out.println(ausgabe);
        return ausgabe;
    }

    
    public ResultSet executeQuery(String sql) {
        try {
            Statement statement = CONNECTION.createStatement();
            ResultSet answer = statement.executeQuery(sql);
            return answer;
        } catch (Exception ex) {
             JOptionPane.showMessageDialog(null,"Fehler beim Ausführen der Query\n"+"Überprüfe die Syntax", "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
        return null;
    }

    public void close() {
        try {
            if (CONNECTION != null) {
                connected = false;
                CONNECTION.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
