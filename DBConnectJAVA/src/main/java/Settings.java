
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Puff Gabriel
 */
public class Settings extends JFrame {
    
    private JPanel mainPanel;
    private JLabel lb_port;
    private JTextField tf_port;
    
    private JLabel lb_dbname;
    private JTextField tf_dbname;
    
    private JLabel lb_user;
    private JTextField tf_user;
    
    private JLabel lb_pwd;
    private JTextField tf_pwd;
    
    
    
    public Settings() {
        initialize();
    }
    
    private void initialize() {
        this.setSize(new Dimension(500, 500));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        mainPanel = new JPanel();
        this.setContentPane(mainPanel);
        mainPanel.setLayout(null);
        
        
        
        
        lb_port = new JLabel();
        lb_port.setText("Port:");
        lb_port.setBounds(25, 10, 100, 35);
        mainPanel.add(lb_port);

        tf_port = new JTextField();
        tf_port.setBounds(100, 10, 250, 35);
        mainPanel.add(tf_port);
        
        
        lb_dbname = new JLabel();
        lb_dbname.setText("DB-Name:");
        lb_dbname.setBounds(25, 45, 100, 35);
        mainPanel.add(lb_dbname);

        tf_dbname = new JTextField();
        tf_dbname.setBounds(100, 45, 250, 35);
        mainPanel.add(tf_dbname);
        
        
        lb_user = new JLabel();
        lb_user.setText("Username:");
        lb_user.setBounds(25, 80, 100, 35);
        mainPanel.add(lb_user);

        tf_user = new JTextField();
        tf_user.setBounds(100, 80, 250, 35);
        mainPanel.add(tf_user);
        
        
        lb_pwd = new JLabel();
        lb_pwd.setText("Password:");
        lb_pwd.setBounds(25, 115, 100, 35);
        mainPanel.add(lb_pwd);

        tf_pwd = new JTextField();
        tf_pwd.setBounds(100, 115, 250, 35);
        mainPanel.add(tf_pwd);
        
        
        
        
    }
    
    
    
    
}
