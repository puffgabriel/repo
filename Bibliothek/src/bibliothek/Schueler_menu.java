/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliothek;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Puff Gabriel
 */
public class Schueler_menu extends JFrame {
    
    private JLabel lbl_ausweisnr;
    private JLabel lbl_nachname;
    private JLabel lbl_vorname;
    
    private JTextField txt_ausweisnr;
    private JTextField txt_nachname;
    private JTextField txt_vorname;
    
    
    private JButton add;
    private JButton search;
    private JButton redo;
    private JButton delete;
    private JButton exit;
    
    
    public Schueler_menu() {
        initialize();
    }

    private JPanel mainPanel;

    private void initialize() {
        this.setSize(new Dimension(600, 400));
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);

        mainPanel = new JPanel();
        this.setContentPane(mainPanel);
        mainPanel.setLayout(null);
        
        //
        lbl_ausweisnr = new JLabel();
        lbl_ausweisnr.setText("Ausweisnummer:");
        lbl_ausweisnr.setBounds(50, 50, 150, 50);
        mainPanel.add(lbl_ausweisnr);
        
        txt_ausweisnr = new JTextField();
        txt_ausweisnr.setBounds(155, 66, 100, 20);
        mainPanel.add(txt_ausweisnr);
        //
        //
        lbl_nachname = new JLabel();
        lbl_nachname.setText("Nachname:");
        lbl_nachname.setBounds(50, 100, 150, 50);
        mainPanel.add(lbl_nachname);
        
        txt_nachname = new JTextField();
        txt_nachname.setBounds(155, 116, 150, 20);
        mainPanel.add(txt_nachname);
        //
        //
        lbl_vorname = new JLabel();
        lbl_vorname.setText("Vorname:");
        lbl_vorname.setBounds(50, 150, 100, 50);
        mainPanel.add(lbl_vorname);
        
        txt_vorname = new JTextField();
        txt_vorname.setBounds(155, 166, 150, 20);
        mainPanel.add(txt_vorname);
        //
        //
        
        Icon icon = new ImageIcon("images/add.png");
        add = new JButton(icon);
        add.setBackground(Color.white);
        add.setBounds(50, 220, 80, 80);
        mainPanel.add(add);

        Icon icon2 = new ImageIcon("images/search.png");
        search = new JButton(icon2);
        search.setBackground(Color.white);
        search.setBounds(140, 220, 80, 80);
        mainPanel.add(search);
        
        Icon icon3 = new ImageIcon("images/redo.png");
        redo = new JButton(icon3);
        redo.setBackground(Color.white);
        redo.setBounds(230, 220, 80, 80);
        mainPanel.add(redo);
        
        Icon icon4 = new ImageIcon("images/delete.png");
        delete = new JButton(icon4);
        delete.setBackground(Color.white);
        delete.setBounds(320, 220, 80, 80);
        mainPanel.add(delete);
        
        Icon icon5 = new ImageIcon("images/exit.png");
        exit = new JButton(icon5);
        exit.setBackground(Color.white);
        exit.setBounds(410, 220, 80, 80);
        mainPanel.add(exit);
        
        
        
        
    }

}
