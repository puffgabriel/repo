/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bibliothek;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Puff Gabriel
 */
public class Buch_menu extends JFrame implements ActionListener {

    public Buch_menu() {
        initialize();
    }
    
    private JPanel mainPanel;
    
    private JLabel lbl_buchnummer;
    private JLabel lbl_buchrueckennummer;
    private JLabel lbl_sachgebiet;
    private JLabel lbl_buchtitel;
    private JLabel lbl_a_nachname;
    private JLabel lbl_a_vorname;
    private JLabel lbl_verlag;
    private JLabel lbl_ort;
    private JLabel lbl_erscheinungsjahr;
    
    private JTextField txt_buchnummer;
    private JTextField txt_buchrueckennummer;
    private JTextField txt_sachgebiet;
    private JTextField txt_buchtitel;
    private JTextField txt_a_nachname;
    private JTextField txt_a_vorname;
    private JTextField txt_verlag;
    private JTextField txt_ort;
    private JTextField txt_erscheinungsjahr;
    
    
    private JButton add;
    private JButton search;
    private JButton redo;
    private JButton delete;
    private JButton exit;
    

    private void initialize() {
        this.setSize(new Dimension(800, 600));
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        
        
        mainPanel = new JPanel();
        this.setContentPane(mainPanel);
        mainPanel.setLayout(null);
        
        //
        lbl_buchnummer = new JLabel();
        lbl_buchnummer.setText("Buchnummer:");
        lbl_buchnummer.setBounds(50, 50, 150, 50);
        mainPanel.add(lbl_buchnummer);
        
        txt_buchnummer = new JTextField();
        txt_buchnummer.setBounds(155, 66, 100, 20);
        mainPanel.add(txt_buchnummer);
        //
        //
        lbl_buchrueckennummer = new JLabel();
        lbl_buchrueckennummer.setText("Buchrückennummer:");
        lbl_buchrueckennummer.setBounds(300, 50, 150, 50);
        mainPanel.add(lbl_buchrueckennummer);
        
        txt_buchrueckennummer = new JTextField();
        txt_buchrueckennummer.setBounds(430, 66, 100, 20);
        mainPanel.add(txt_buchrueckennummer);
        //
        //
        lbl_sachgebiet = new JLabel();
        lbl_sachgebiet.setText("Sachgebiet:");
        lbl_sachgebiet.setBounds(50, 100, 150, 50);
        mainPanel.add(lbl_sachgebiet);
        
        txt_sachgebiet = new JTextField();
        txt_sachgebiet.setBounds(155, 116, 300, 20);
        mainPanel.add(txt_sachgebiet);
        //
        //
        lbl_buchtitel = new JLabel();
        lbl_buchtitel.setText("Buchtitel:");
        lbl_buchtitel.setBounds(50, 150, 150, 50);
        mainPanel.add(lbl_buchtitel);
        
        txt_buchtitel = new JTextField();
        txt_buchtitel.setBounds(155, 166, 300, 20);
        mainPanel.add(txt_buchtitel);
        //
        //
        lbl_a_nachname = new JLabel();
        lbl_a_nachname.setText("Autor-Nachname:");
        lbl_a_nachname.setBounds(50, 200, 150, 50);
        mainPanel.add(lbl_a_nachname);
        
        txt_a_nachname = new JTextField();
        txt_a_nachname.setBounds(155, 216, 100, 20);
        mainPanel.add(txt_a_nachname);
        //
        //
        lbl_a_vorname = new JLabel();
        lbl_a_vorname.setText("Vorname:");
        lbl_a_vorname.setBounds(300, 200, 100, 50);
        mainPanel.add(lbl_a_vorname);
        
        txt_a_vorname = new JTextField();
        txt_a_vorname.setBounds(400, 216, 100, 20);
        mainPanel.add(txt_a_vorname);
        //
        //
        lbl_verlag = new JLabel();
        lbl_verlag.setText("Verlag:");
        lbl_verlag.setBounds(50, 250, 150, 50);
        mainPanel.add(lbl_verlag);
        
        txt_verlag = new JTextField();
        txt_verlag.setBounds(155, 266, 100, 20);
        mainPanel.add(txt_verlag);
        //
        //
        lbl_ort = new JLabel();
        lbl_ort.setText("Ort:");
        lbl_ort.setBounds(300, 250, 150, 50);
        mainPanel.add(lbl_ort);
        
        txt_ort = new JTextField();
        txt_ort.setBounds(400, 266, 100, 20);
        mainPanel.add(txt_ort);
        //
        //
        lbl_erscheinungsjahr = new JLabel();
        lbl_erscheinungsjahr.setText("Erscheinungsjahr:");
        lbl_erscheinungsjahr.setBounds(50, 300, 150, 50);
        mainPanel.add(lbl_erscheinungsjahr);
        
        txt_erscheinungsjahr = new JTextField();
        txt_erscheinungsjahr.setBounds(155, 316, 100, 20);
        mainPanel.add(txt_erscheinungsjahr);
        
        
        
        
        
        
        
        //btn_panel bp = new btn_panel();
        //mainPanel.add(bp);
        
        Icon icon = new ImageIcon("images/add.png");
        add = new JButton(icon);
        add.setBackground(Color.white);
        add.setBounds(50, 370, 80, 80);
        mainPanel.add(add);

        Icon icon2 = new ImageIcon("images/search.png");
        search = new JButton(icon2);
        search.setBackground(Color.white);
        search.setBounds(140, 370, 80, 80);
        mainPanel.add(search);
        
        Icon icon3 = new ImageIcon("images/redo.png");
        redo = new JButton(icon3);
        redo.setBackground(Color.white);
        redo.setBounds(230, 370, 80, 80);
        mainPanel.add(redo);
        
        Icon icon4 = new ImageIcon("images/delete.png");
        delete = new JButton(icon4);
        delete.setBackground(Color.white);
        delete.setBounds(320, 370, 80, 80);
        mainPanel.add(delete);
        
        Icon icon5 = new ImageIcon("images/exit.png");
        exit = new JButton(icon5);
        exit.setBackground(Color.white);
        exit.setBounds(410, 370, 80, 80);
        mainPanel.add(exit);
        
        
        
        
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == add) {
            DBconn conn = new DBconn();

            if (!conn.isConnected()) {
                if (conn.connect()) {
                    String sql = "insert into buch (sachgebiet, buchtitel, a_nachname, a_vorname, verlag, erscheinungsjahr, ort) values ('"+lbl_sachgebiet.getText()+"','"+lbl_buchtitel.getText()+"','"+lbl_a_nachname.getText()+"','"+lbl_a_vorname.getText()+"','"+lbl_verlag.getText()+"','01/01/"+lbl_erscheinungsjahr.getText()+"','"+lbl_ort.getText()+"');";
                    if(conn.executeUpdate(sql)>=0){
                            JOptionPane.showMessageDialog(this, "Datensatz erfolgreich eingefügt.", "Info", JOptionPane.INFORMATION_MESSAGE);
                    }else{
                            JOptionPane.showMessageDialog(this, "Datensatz konnte nicht eingefügt werden.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    conn.close();
                } else {
                    JOptionPane.showMessageDialog(this, "Verbindung zur Datenbank fehlgeschlagen.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }else{
            System.out.println("Fehler");
        }
    }
    
    
    
    

}
