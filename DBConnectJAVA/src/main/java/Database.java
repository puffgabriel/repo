
import java.sql.Connection;
import java.sql.DriverManager;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Puff Gabriel
 */
public class Database {

//    public static void main(String[] args) {
//        
//            Database db = new Database();
//            Connection conn = db.getConnection();
//            if (conn == null) {
//                System.out.println("Es ist ein Fehler aufgetreten");
//            }else{
//                System.out.println("Verbindung erfolgreich");
//            }
//            try {
//            Statement st = conn.createStatement();
//            String sql = "SELECT * FROM MITARBEITER";
//            ResultSet rs = st.executeQuery(sql);
//                while (rs.next()) {                    
//                    int columns = rs.getMetaData().getColumnCount();
//                    for (int i = 1; i <= columns; i++) {
//                        System.out.print(rs.getObject(i).toString() + " | ");
//                    }
//                    System.out.println("");
//                }
//                st.close();
//                conn.close();
//            
//        } catch (SQLException ex) {
//            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }

    public Connection getConnection() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Schokoladenfabrik", "postgres", "root");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }

}
